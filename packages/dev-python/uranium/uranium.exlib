# Copyright 2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PN=Uranium

require github [ user=Ultimaker ] cmake
require python [ blacklist=2 multibuild=false ]

export_exlib_phases src_prepare src_compile src_test src_install

SUMMARY="A Python framework for building 3D printing related applications"

LICENCES="LGPL-3"
SLOT="0"
MYOPTIONS="doc"

DEPENDENCIES="
    build:
        doc? ( app-doc/doxygen )
    build+run:
        dev-python/numpy[python_abis:*(-)?]
        dev-python/PyQt5[python_abis:*(-)?]
        dev-python/scipy[python_abis:*(-)?]
        dev-python/shapely[python_abis:*(-)?]
        net-libs/libarcus[python_abis:*(-)?]
    test:
        dev-python/pytest[python_abis:*(-)?]
"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DCURA_BINARY_DATA_DIRECTORY:PATH="/usr/share/cura/"
    -DPython3_EXECUTABLE=${PYTHON}
    -DGETTEXT_MSGINIT_EXECUTABLE="msginit"
)

uranium_src_prepare() {
    cmake_src_prepare

    # We install cmake modules to a unversioned cmake dir
    edo sed \
        -e 's|cmake-${CMAKE_MAJOR_VERSION}.${CMAKE_MINOR_VERSION}|cmake|' \
        -i CMakeLists.txt
}

uranium_src_compile() {
    default

    option doc && emake doc
}

uranium_src_test() {
    # pytest-main wants to bind to 0.0.0.0@8080
    # cod-style is a bit like -Werror, also saves us a mypy dep
    edo ctest -E '(pytest-main|code-style)' --verbose
}

uranium_src_install() {
    cmake_src_install

    if option doc ; then
        edo pushd "${CMAKE_SOURCE}"
        dodoc -r html
        edo popd
    fi
}

