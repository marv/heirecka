# Copyright 2017-2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde.org kde freedesktop-desktop gtk-icon-cache

export_exlib_phases src_prepare pkg_postinst pkg_postrm

SUMMARY="A heap memory profiler for Linux"
DESCRIPTION="
Heaptrack traces all memory allocations and annotates these events with stack
traces. Dedicated analysis tools then allow you to interpret the heap memory
profile to:

* find hotspots that need to be optimized to reduce the memory footprint of
  your application
* find memory leaks, i.e. locations that allocate memory which is never
  deallocated
* find allocation hotspots, i.e. code locations that trigger a lot of memory
  allocation calls
* find temporary allocations, which are allocations that are directly
  followed by their deallocation
"
HOMEPAGE="https://userbase.kde.org/Heaptrack"

LICENCES="
    Boost-1.0 [[ note = [ 3rdparty/{boost-zstd,catch.hpp} ] ]]
    BSD-3     [[ note = [ cmake scripts ] ]]
    LGPL-2.1
    MIT   [[ note = [ libbacktrace, https://github.com/ValveSoftware/vogl ] ]]
"
SLOT="0"
MYOPTIONS="
    gui [[ description = [ Build a Qt5/KF5 based GUI for heaptrack ] ]]
"

DEPENDENCIES="
    build+run:
        app-arch/zstd
        dev-libs/boost[>=1.41.0]
        dev-libs/libunwind
        sys-libs/zlib
        gui? (
            kde/kdiagram[>=2.6.0]
            kde-frameworks/kconfigwidgets:5
            kde-frameworks/kcoreaddons:5
            kde-frameworks/ki18n:5
            kde-frameworks/kio:5
            kde-frameworks/kitemmodels:5
            kde-frameworks/threadweaver:5
            x11-libs/qtbase:5[>=5.2.0][gui]
        )
"
# NOTE: Bundles a copy of boost's iostream with support for zstd:
#        https://github.com/rdoeffinger/iostreams/tree/zstd

UPSTREAM_RELEASE_NOTES="https://www.kdab.com/heaptrack-version-1-2-0-released/"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DLOCALE_INSTALL_DIR:PATH="/usr/share/locale"
    -DHEAPTRACK_BUILD_BACKTRACE:BOOL=TRUE
)
CMAKE_SRC_CONFIGURE_OPTIONS+=(
    'gui HEAPTRACK_BUILD_GUI'
)

heaptrack_src_prepare() {
    kde_src_prepare

    # Test fails under sydbox
    edo sed -e "/add_test(NAME tst_inject COMMAND tst_inject)/d" \
            -i tests/auto/CMakeLists.txt
}

heaptrack_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

heaptrack_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}
